#include "plugin.h"
//#include <iostream>
//#include <string>
//#include <termios.h>
//#include <unistd.h>

Plugin::Plugin() :
	QObject(nullptr),
	PluginBase(this),
	m_cliElement(new CLIElement(this, "task", "[add NAME [ATTRIBUTE:VALUE] | update ID ATTRIBUTE:VALUE | remove ID]"))
{
	initPluginBase(
	{
		{INTERFACE(IPlugin), this},
		{INTERFACE(ICLIElement), m_cliElement}
	},
	{
		{INTERFACE(IUserTaskDataExtention), m_taskManager},
	});
	connect(m_cliElement, &CLIElement::onHandle, this, &Plugin::handle);
}

void Plugin::onReady()
{
	auto model = m_taskManager->getModel();
	m_userTasksFilter = model->getFilter();
}

ExtendableItemDataMap Plugin::getAttributes(const QStringList& args, QPointer<IExtendableDataModel> model)
{
	Interface interface = INTERFACE(IUserTaskDataExtention);
	QString attribute = "name";
	QString value;
	ExtendableItemDataMap attributes;
	auto header = model->getHeader();
	auto iter = args.begin()+1;
	while(iter != args.end())
	{
		const auto& arg = *iter;
		int colonIndex = arg.indexOf(':');
		if(colonIndex != -1)
		{
			attribute = arg.sliced(0, colonIndex);
			for(auto interfaceIter = header.begin(); interfaceIter != header.end(); ++interfaceIter)
			{
				if(interfaceIter.value().contains(attribute))
				{
					interface = interfaceIter.key().iid();
					break;
				}
			}
			if(interface.iid().isEmpty())
			{
				qDebug() << "Not found data extention for attribute" << attribute;
				continue;
			}
			attributes[interface][attribute] = value;
			value = arg.sliced(colonIndex+1);
		}
		else
		{
			value += *iter;
		}
		++iter;
		if(iter != args.end())
		{
			value += ' ';
		}
	}
	attributes[interface][attribute] = value;
	return attributes;
}

void Plugin::handle(const QStringList& args)
{
	auto model = m_taskManager->getModel();
	if(!args.length())
	{
		auto ids = model->getItemIds();
		for(auto& id : ids)
			showTask(id);
		return;
	}

	const auto& action = args.at(0);
	if(action == "add")
	{
		if(args.length() < 2)
		{
			qDebug() << "No task name specified";
			return;
		}
		auto attributes = getAttributes(args, model);
		auto id = model->appendItem(attributes);
		showTask(id);
		return;
	}

	int itemId = args[1].toInt();
	if(!model->hasItem(itemId))
	{
		qDebug() << "No such task";
		return;
	}
	if(action == "update")
	{
		auto item = model->getItem(itemId);
		ExtendableItemDataMap attributes = getAttributes(args.sliced(1), model);
		for(auto iter = attributes.begin(); iter != attributes.end(); ++ iter)
		{
			item[iter.key()].insert(iter.value());
		}
		model->updateItem(itemId, item);
	}
	else if(action == "remove")
	{
		model->removeItem(itemId);
	}
}

void Plugin::showTask(int itemId)
{
	auto model = m_taskManager->getModel();
	if(!model->hasItem(itemId))
		return;
	auto item = model->getItem(itemId);
	const auto& dataInterface = INTERFACE(IUserTaskDataExtention);
	const auto& data = item[dataInterface];
	qDebug() << Qt::endl << QString("[%1] %2").arg(itemId).arg(data["name"].toString());
	for(auto iterExt = item.begin(); iterExt != item.end(); ++iterExt)
	{
		if(iterExt.key() == dataInterface)
			continue;
		auto iter = iterExt.value().begin();
		while(iter != iterExt.value().end())
		{
			qDebug() << QString("%1: %2").arg(iter.key()).arg(iter.value().toString());
			++iter;
		}
	}
}
